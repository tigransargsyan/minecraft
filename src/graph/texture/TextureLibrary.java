package graph.texture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class TextureLibrary {
	public int textureWidth;
	public int textureHeight;
	public int libraryWidth;
	public int libraryHeight;
	public Bitmap[] textures;
	public int nColumns;
	public int nRows;
	public int nTextures;
	
	public TextureLibrary(int textureWidth, int textureHeight, String libraryFile) {
		this.textureWidth = textureWidth;
		this.textureHeight = textureHeight;
		
		loadTextures(libraryFile);
	}
	
	public boolean loadTextures(String libraryFile) {
		BufferedImage img = null;
        try {
            img = ImageIO.read(new File(libraryFile));
        } catch (IOException e) {
        	System.out.println(e.getStackTrace());
        }
        
        libraryWidth = img.getWidth();
        libraryHeight = img.getHeight();
        
        nColumns = libraryWidth / textureWidth;
        nRows = libraryHeight / textureHeight;
        nTextures = nColumns * nRows;
        
        textures = new Bitmap[nTextures];
        
        int textureIdx;
        for(int column = 0; column < nColumns; column++) {
        	for(int row = 0; row < nRows; row++) {
        		textureIdx =  column + row * nColumns;
        		textures[textureIdx] = new Bitmap(textureWidth, textureHeight);
        		System.out.println("Loading texture [" + column+ "," + row +"]");
        		for(int x = column * textureWidth, x1 = 0; x < column * textureWidth + textureWidth; x++, x1++) {
        			for(int y = row * textureHeight, y1 = 0; y < row * textureHeight + textureHeight; y++, y1++) {
        				textures[textureIdx].pixels[x1 + y1 * textureWidth] = img.getRGB(x, y);
        			}
        		}
        	}
        }
        return true;
	}
	
	public Bitmap getTexture(int column, int row) {
		return textures[column + row * nColumns];
	}
	
	public Bitmap getTexture(int item) {
		return textures[item];
	}
}
