package graph.texture;
import math.Vector2i;

public class Font {
	public TextureLibrary font;
	
	public Font(String resourceFile) {
		font = new TextureLibrary(8, 8, resourceFile);
	}
	
	public Bitmap getChar(char ch) {
		return font.getTexture(ch);
	}
	
	public void renderText(Vector2i position, String string, Bitmap view) {
		for(int i = 0; i < string.length(); i++)
			view.draw(getChar(string.charAt(i)), position.x + i*8, position.y);
	}
}
