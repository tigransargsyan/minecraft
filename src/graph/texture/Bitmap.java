package graph.texture;
import math.Vector2i;

public class Bitmap {
	public final int width;
	public final int height;
	public final int[] pixels;
	
	public Bitmap(int width, int height){
		this.width = width;
		this.height = height;
		
		pixels = new int[width*height];
	}
	
	
	public void draw(Bitmap map, int xOffset, int yOffset){
		int xCorrected;
		int yCorrected;
		
		for(int x = 0; x < map.width; x++){
			xCorrected = x + xOffset;
			if(xCorrected < 0 || xCorrected >= width) continue;
			for(int y = 0; y < map.height; y++){
				yCorrected = y + yOffset;
				if(yCorrected < 0 || yCorrected >= height) continue;
				
//				if(map.pixels[x + y*map.width] == 0x00FFFFFF) {
//					//pixels[xCorrected + yCorrected*width ] = 0xFFFF0000; /* RED */ /* TODO */
//					continue;
//				} else if(map.pixels[x + y*map.width] == 0x00000000) {
//					pixels[xCorrected + yCorrected*width ] = 0xFF00FF00; /* GREEN */ /* TODO */
//					continue;
//				}
				pixels[xCorrected + yCorrected*width ] = map.pixels[x + y*map.width];
			}
		}
	}
	
	public void drawRect(Vector2i a, Vector2i b, Vector2i c, Vector2i d, int color){
		drawTriangleOpt1(a, b, c, color);
		drawTriangleOpt1(a, c, d, color);
	}
	
	public void drawTriangle(Vector2i v1, Vector2i v2, Vector2i v3, int color) {
		/* get the bounding box of the triangle */
		int maxX = Math.max(v1.x, Math.max(v2.x, v3.x));
		int minX = Math.min(v1.x, Math.min(v2.x, v3.x));
		int maxY = Math.max(v1.y, Math.max(v2.y, v3.y));
		int minY = Math.min(v1.y, Math.min(v2.y, v3.y));
		
		/* spanning vectors of edge (v1,v2) and (v1,v3) */
		Vector2i vs1 = new Vector2i(v2.x - v1.x, v2.y - v1.y);
		Vector2i vs2 = new Vector2i(v3.x - v1.x, v3.y - v1.y);

		for (int x = minX; x <= maxX; x++)
		{
		  for (int y = minY; y <= maxY; y++)
		  {
		    Vector2i q = new Vector2i(x - v1.x, y - v1.y);

		    double s = Vector2i.crossProduct(q, vs2) / Vector2i.crossProduct(vs1, vs2);
		    double t = Vector2i.crossProduct(vs1, q) / Vector2i.crossProduct(vs1, vs2);

		    if ( (s >= 0) && (t >= 0) && (s + t <= 1))
		    { /* inside triangle */
		    	pixels[x + y*width] = color;
		    }
		  }
		}
	}
	
    protected void fillFlatSideTriangleInt(Vector2i v1, Vector2i v2, Vector2i v3, int color)
    {
    	Vector2i vTmp1 = new Vector2i(v1.x, v1.y);
    	Vector2i vTmp2 = new Vector2i(v1.x, v1.y);
        
        boolean changed1 = false;
        boolean changed2 = false;
        
        int dx1 = Math.abs(v2.x - v1.x);
        int dy1 = Math.abs(v2.y - v1.y);
        
        int dx2 = Math.abs(v3.x - v1.x);
        int dy2 = Math.abs(v3.y - v1.y);
        
        int signx1 = (int)Math.signum(v2.x - v1.x);
        int signx2 = (int)Math.signum(v3.x - v1.x);
        
        int signy1 = (int)Math.signum(v2.y - v1.y);
        int signy2 = (int)Math.signum(v3.y - v1.y);
        
        if (dy1 > dx1)
        {   // swap values
            int tmp = dx1;
            dx1 = dy1;
            dy1 = tmp;
            changed1 = true;
        }
        
        if (dy2 > dx2)
        {   // swap values
            int tmp = dx2;
            dx2 = dy2;
            dy2 = tmp;
            changed2 = true;
        }
        
        int e1 = 2 * dy1 - dx1;
        int e2 = 2 * dy2 - dx2;
        
        for (int i = 0; i <= dx1; i++)
        {
        	
            drawLine(vTmp1.x, vTmp1.y, vTmp2.x, vTmp2.y, color);
            
            while (e1 >= 0)
            {
                if (changed1)
                    vTmp1.x += signx1;
                else
                    vTmp1.y += signy1;
                e1 = e1 - 2 * dx1;
            }
            
            if (changed1)
                vTmp1.y += signy1;
            else
                vTmp1.x += signx1;  
          
            e1 = e1 + 2 * dy1;
            
            /* here we rendered the next point on line 1 so follow now line 2
             * until we are on the same y-value as line 1.
             */
            while (vTmp2.y != vTmp1.y)
            {
                while (e2 >= 0)
                {
                    if (changed2)
                        vTmp2.x += signx2;
                    else
                        vTmp2.y += signy2;
                    e2 = e2 - 2 * dx2;
                }

                if (changed2)
                    vTmp2.y += signy2;
                else
                    vTmp2.x += signx2;

                e2 = e2 + 2 * dy2;
            }
        }
        
    }
	
	public void drawTriangleOpt(Vector2i vt1, Vector2i vt2, Vector2i vt3, int color) {
	        
        Vector2i vTmp;
        
        if (vt1.y > vt2.y)
        {
            vTmp = vt1;
            vt1 = vt2;
            vt2 = vTmp;
        }
        /* here v1.y <= v2.y */
        if (vt1.y > vt3.y)
        {
            vTmp = vt1;
            vt1 = vt3;
            vt3 = vTmp;
        }
        /* here v1.y <= v2.y and v1.y <= v3.y so test v2 vs. v3 */
        if (vt2.y > vt3.y)
        {
            vTmp = vt2;
            vt2 = vt3;
            vt3 = vTmp;
        }
	        
        /* here we know that v1.y <= v2.y <= v3.y */
        /* check for trivial case of bottom-flat triangle */
        if (vt2.y == vt3.y)
        {
            fillFlatSideTriangleInt(vt1, vt2, vt3, color);
        }
        /* check for trivial case of top-flat triangle */
        else if (vt1.y == vt2.y)
        {
            fillFlatSideTriangleInt(vt3, vt1, vt2, color);
        } 
        else
        {
            /* general case - split the triangle in a topflat and bottom-flat one */
            Vector2i vTmp1 = new Vector2i( 
                (int)(vt1.x + ((float)(vt2.y - vt1.y) / (float)(vt3.y - vt1.y)) * (vt3.x - vt1.x)), vt2.y);
            fillFlatSideTriangleInt(vt1, vt2, vTmp1, color);
            fillFlatSideTriangleInt(vt3, vt2, vTmp1, color);
        }
	}
	
    /**
     * Fills a triangle whose bottom side is perfectly horizontal.
     * Precondition is that v2 and v3 perform the flat side and that v1.y < v2.y, v3.y.
     * @param g Graphics object 
     * @param v1 first vertice, has the smallest y-coordinate
     * @param v2 second vertice
     * @param v3 third vertice
     */
    private void fillBottomFlatTriangle(Vector2i v1, Vector2i v2, Vector2i v3, int color)
    {
        float slope1 = (float)(v2.x - v1.x) / (float)(v2.y - v1.y);
        float slope2 = (float)(v3.x - v1.x) / (float)(v3.y - v1.y);

        float x1 = v1.x;
        float x2 = v1.x + 0.5f;

        for (int scanlineY = v1.y; scanlineY <= v2.y; scanlineY++)
        {
            drawLine((int)x1, scanlineY, (int)x2, scanlineY, color);
            x1 += slope1;
            x2 += slope2;

        }
    }
    
    /**
     * Fills a triangle whose top side is perfectly horizontal.
     * Precondition is that v1 and v2 perform the flat side and that v3.y > v1.y, v2.y.
     * @param g Graphics object 
     * @param v1 first vertice
     * @param v2 second vertice
     * @param v3 third vertice, has the largest y-coordinate
     */
    protected void fillTopFlatTriangle(Vector2i v1, Vector2i v2, Vector2i v3, int color)
    {
        float slope1 = (float)(v3.x - v1.x) / (float)(v3.y - v1.y);
        float slope2 = (float)(v3.x - v2.x) / (float)(v3.y - v2.y);

        float x1 = v3.x;
        float x2 = v3.x + 0.5f;

        for (int scanlineY = v3.y; scanlineY > v1.y; scanlineY--)
        {
            x1 -= slope1;
            x2 -= slope2;
            drawLine((int)x1, scanlineY, (int)x2, scanlineY, color);
        }
    }

    public void drawTriangleOpt1(Vector2i vt1, Vector2i vt2, Vector2i vt3, int color) {
        
        Vector2i vTmp;
        
        if (vt1.y > vt2.y)
        {
            vTmp = vt1;
            vt1 = vt2;
            vt2 = vTmp;
        }
        /* here v1.y <= v2.y */
        if (vt1.y > vt3.y)
        {
            vTmp = vt1;
            vt1 = vt3;
            vt3 = vTmp;
        }
        /* here v1.y <= v2.y and v1.y <= v3.y so test v2 vs. v3 */
        if (vt2.y > vt3.y)
        {
            vTmp = vt2;
            vt2 = vt3;
            vt3 = vTmp;
        }
        
        /* here we know that v1.y <= v2.y <= v3.y */
        /* check for trivial case of bottom-flat triangle */
        if (vt2.y == vt3.y)
        {
            fillBottomFlatTriangle(vt1, vt2, vt3, color);
        }
        /* check for trivial case of top-flat triangle */
        else if (vt1.y == vt2.y)
        {
            fillTopFlatTriangle(vt1, vt2, vt3, color);
        } 
        else
        {
            /* general case - split the triangle in a topflat and bottom-flat one */
            Vector2i vTmp1 = new Vector2i( 
                (int)(vt1.x + ((float)(vt2.y - vt1.y) / (float)(vt3.y - vt1.y)) * (vt3.x - vt1.x)), vt2.y);
            fillBottomFlatTriangle(vt1, vt2, vTmp1, color);
            fillTopFlatTriangle(vt2, vTmp1, vt3, color);
        }
        
    }
	
	public void drawMarker(Vector2i point, int color){
		int idx;
		for(int x = point.x-2; x <= point.x+2; x++) {
			for(int y = point.y-2; y <= point.y+2; y++) {
				idx = x + y*width;
				if(idx > 0 && idx < width*height)
					pixels[idx] = color;
			}
		}
	}
	
	public void drawLine(Vector2i start, Vector2i end, int color){
		drawLine(start.x, start.y, end.x, end.y, color);
	}
	
	public void drawLine(int startX, int startY, int endX, int endY, int color){
		int x, y, dx, dy, p, incE, incNE, stepx, stepy;
		
		  dx = (endX - startX);
		  dy = (endY - startY);
		 /* determinar que punto usar para empezar, cual para terminar */
		  if (dy < 0) { 
		    dy = -dy; stepy = -1; 
		  } 
		  else
		    stepy = 1;
		  if (dx < 0) {  
		    dx = -dx; stepx = -1; 
		  } 
		  else 
		    stepx = 1;
		  x = startX;
		  y = startY;
		  
		  pixels[x + y * width] = color;
		 /* se cicla hasta llegar al extremo de la l�nea */
		  if(dx>dy){
		    p = 2*dy - dx;
		    incE = 2*dy;
		    incNE = 2*(dy-dx);
		    while (x != endX){
		      x = x + stepx;
		      if (p < 0){
		        p = p + incE;
		      }
		      else {
		        y = y + stepy;
		        p = p + incNE;
		      }
		      pixels[x + y * width] = color;
		    }
		  }
		  else{
		    p = 2*dx - dy;
		    incE = 2*dx;
		    incNE = 2*(dx-dy);
		    while (y != endY){
		      y = y + stepy;
		      if (p < 0){
		        p = p + incE;
		      }
		      else {
		        x = x + stepx;
		        p = p + incNE;
		      }
		      pixels[x + y * width] = color;
		    }
		  }		
	}
	
	public void clean(){
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				pixels[x + y*width ] = 0;
			}
		}		
	}
}