package graph;

import game.cubes.Cube;
import game.cubes.Face;
import graph.texture.Bitmap;
import math.Vector2i;
import math.Vector3f;

public class DrawUtils {
	
//	static public void drawBitmapIm(Bitmap bitmap, Vector3f a, Vector3f b, Vector3f c, Vector3f d) {
//		int viewPortWidth = viewPort.width;
//		int viewPortHeight = viewPort.height;
//		
//		Vector2i pa, pb, pc, pd;
//		
//		pa =  camera.projectCanvas(a, viewPortWidth, viewPortHeight);
//		pb =  camera.projectCanvas(b, viewPortWidth, viewPortHeight);
//		pc =  camera.projectCanvas(c, viewPortWidth, viewPortHeight);
//		pd =  camera.projectCanvas(d, viewPortWidth, viewPortHeight);
//		
//		if(pa != null && pb != null && pc != null && pd != null) {
//			
//			Vector2f vx = new Vector2f(pc.x - pb.x, pc.y - pb.y);
//			Vector2f vy = new Vector2f(pa.x - pb.x, pa.y - pb.y);
//			
//			double vxModule = vx.module();
//			double vyModule = vy.module();
//			
//			/* Normalizar vx y vy */
//			vx.x /= vxModule;
//			vx.y /= vxModule;
//
//			vy.x /= vyModule;
//			vy.y /= vyModule;
//			
//			double xStep = vxModule / bitmap.width;
//			double yStep = vyModule / bitmap.height;
//			
//			Vector2i point1, point2, point3, point4;
//			double x = 0;
//			double y;
//			for(int i = 0; i < bitmap.width; i++) {
//				y = 0;
//				for(int j = 0; j < bitmap.height; j++) {
//					point1 = new Vector2i((int) (pb.x + vx.x*x +vy.x*y),         			(int) (pb.y + vx.y*x +         vy.y*y));
//					point2 = new Vector2i((int) (pb.x + vx.x*(x) + vy.x*(y+yStep)), 		(int) (pb.y + vx.y*(x) + vy.y*(y+yStep)));
//					point3 = new Vector2i((int) (pb.x + vx.x*(x+xStep) + vy.x*(y+yStep)), 	(int) (pb.y + vx.y*(x+xStep) + vy.y*(y+yStep)));
//					point4 = new Vector2i((int) (pb.x + vx.x*(x+xStep) + vy.x*(y)), 		(int) (pb.y + vx.y*(x+xStep) + vy.y*(y)));
//					
//					if(	point1.x >= 0 && point1.x < viewPort.width &&
//						point2.x >= 0 && point2.x < viewPort.width &&
//						point3.x >= 0 && point3.x < viewPort.width &&
//						point4.x >= 0 && point4.x < viewPort.width &&
//						point1.y >= 0 && point1.y < viewPort.height &&
//						point2.y >= 0 && point2.y < viewPort.height &&
//						point3.y >= 0 && point3.y < viewPort.height &&
//						point4.y >= 0 && point4.y < viewPort.height) {
//						
//						viewPort.drawRect(point1, point2, point3, point4, bitmap.pixels[i + j*bitmap.width]);
////						viewPort.drawLine(point1, point2, bitmap.pixels[i + j*bitmap.width]);
////						viewPort.drawLine(point2, point3, bitmap.pixels[i + j*bitmap.width]);
////						viewPort.drawLine(point3, point4, bitmap.pixels[i + j*bitmap.width]);
////						viewPort.drawLine(point4, point1, bitmap.pixels[i + j*bitmap.width]);
//						//viewPort.drawLine(pointInf, pointInf, 0xFFFF00FF);
//
//					}
//					y += yStep;
//				}
//				x += xStep;
//			}
//			viewPort.drawLine(pa, pb, 0xFFFF0000);
//			viewPort.drawLine(pb, pc, 0xFFFF0000);
//			viewPort.drawLine(pc, pd, 0xFFFF0000);
//			viewPort.drawLine(pd, pa, 0xFFFF0000);
//			
//			viewPort.drawMarker(pa, 0xFFFFFF00);
//			viewPort.drawMarker(pb, 0xFFFFFF00);
//			viewPort.drawMarker(pc, 0xFFFFFF00);
//			viewPort.drawMarker(pd, 0xFFFFFF00);
//		}
//	}
	
	static public void drawBitmap(Bitmap viewPort, Camera camera, Bitmap texture, Vector3f a, Vector3f b, Vector3f c, Vector3f d) {
		int viewPortWidth = viewPort.width;
		int viewPortHeight = viewPort.height;
		
		Vector3f vx = new Vector3f(d.x - a.x, d.y - a.y, d.z - a.z);
		Vector3f vy = new Vector3f(b.x - a.x, b.y - a.y, b.z - a.z);
		double vxModule = vx.module();
		double vyModule = vy.module();
		
		/* Normalizar vx y vy */
		vx.x /= vxModule;
		vx.y /= vxModule;
		vx.z /= vxModule;

		vy.x /= vyModule;
		vy.y /= vyModule;
		vy.z /= vyModule;
		
		double xStep = vxModule / texture.width;
		double yStep = vyModule / texture.height;
		
		/* Perform a horizontal and vertical scans */
		Vector3f a3, b3, c3, d3;
		Vector2i a2, b2, c2, d2;
		int i = 0, j;
		for(double x = 0; x <= vxModule - xStep; x += xStep) {
			j = 0;
			for(double y = 0; y <= vyModule - yStep; y += yStep) {
				a3 = new Vector3f(a.x + vx.x*x +         vy.x*y,         a.y + vx.y*x +         vy.y*y,         a.z + vx.z*x +         vy.z*y);
				b3 = new Vector3f(a.x + vx.x*x + 		 vy.x*(y+yStep), a.y + vx.y*x + 		vy.y*(y+yStep), a.z + vx.z*x + 		   vy.z*(y+yStep));
				c3 = new Vector3f(a.x + vx.x*(x+xStep) + vy.x*(y+yStep), a.y + vx.y*(x+xStep) + vy.y*(y+yStep), a.z + vx.z*(x+xStep) + vy.z*(y+yStep));
				d3 = new Vector3f(a.x + vx.x*(x+xStep) + vy.x*y, 		 a.y + vx.y*(x+xStep) + vy.y*y, 		a.z + vx.z*(x+xStep) + vy.z*y);
				
				a2 =  camera.projectCanvas(a3, viewPortWidth, viewPortHeight);
				b2 =  camera.projectCanvas(b3, viewPortWidth, viewPortHeight);
				c2 =  camera.projectCanvas(c3, viewPortWidth, viewPortHeight);
				d2 =  camera.projectCanvas(d3, viewPortWidth, viewPortHeight);
				
				if(a2 != null && b2 != null && c2 != null && d2 != null) {
					
					viewPort.drawRect(a2, b2, c2, d2, texture.pixels[i + j*texture.width]);					
//					viewPort.drawLine(a2,  b2, bitmap.pixels[i + j*bitmap.width]);
//					viewPort.drawLine(b2,  c2, bitmap.pixels[i + j*bitmap.width]);
//					viewPort.drawLine(c2,  d2, bitmap.pixels[i + j*bitmap.width]);
//					viewPort.drawLine(d2,  a2, bitmap.pixels[i + j*bitmap.width]);
				}
				j++;
			}
			i++;
		}
	}
	
	
	
	static public void drawWireframeCube(Bitmap viewPort, Camera camera, Cube cube) {
		Vector2i a, b, c, d, e, f, g, h;
		
//		int color1 = 0xFFFFFF;
//		
//		int color2 = 0x888888;
//		
//		int color3 = 0xAAAAAA;
		
		int color1 = 0xFFFF00;
		
		int color2 = 0xFFFF00;
		
		int color3 = 0xFFFF00;
		
		int viewPortWidth = viewPort.width;
		int viewPortHeight = viewPort.height;
		
		a = camera.projectCanvas(cube.a, viewPortWidth, viewPortHeight);
		b = camera.projectCanvas(cube.b, viewPortWidth, viewPortHeight);
		c = camera.projectCanvas(cube.c, viewPortWidth, viewPortHeight);
		d = camera.projectCanvas(cube.d, viewPortWidth, viewPortHeight);
		
		e = camera.projectCanvas(cube.e, viewPortWidth, viewPortHeight);
		f = camera.projectCanvas(cube.f, viewPortWidth, viewPortHeight);
		g = camera.projectCanvas(cube.g, viewPortWidth, viewPortHeight);
		h = camera.projectCanvas(cube.h, viewPortWidth, viewPortHeight);
		
		if(a != null && b != null) viewPort.drawLine(a, b, color1);
		if(b != null && c != null) viewPort.drawLine(b, c, color1);
		if(c != null && d != null) viewPort.drawLine(c, d, color1);
		if(d != null && a != null) viewPort.drawLine(d, a, color1);
		
		if(e != null && f != null) viewPort.drawLine(e, f, color2);
		if(f != null && g != null) viewPort.drawLine(f, g, color2);
		if(g != null && h != null) viewPort.drawLine(g, h, color2);
		if(h != null && e != null) viewPort.drawLine(h, e, color2);
		
		if(a != null && e != null) viewPort.drawLine(a, e, color3);
		if(b != null && f != null) viewPort.drawLine(b, f, color3);
		if(c != null && g != null) viewPort.drawLine(c, g, color3);
		if(d != null && h != null) viewPort.drawLine(d, h, color3);
	}
	
	static public void drawWireframeFace(Bitmap viewPort, Camera camera, Face face) {
		Vector2i a, b, c, d;
		
		int color = 0xFF0000;
		
		int viewPortWidth = viewPort.width;
		int viewPortHeight = viewPort.height;
		
		a = camera.projectCanvas(face.a, viewPortWidth, viewPortHeight);
		b = camera.projectCanvas(face.b, viewPortWidth, viewPortHeight);
		c = camera.projectCanvas(face.c, viewPortWidth, viewPortHeight);
		d = camera.projectCanvas(face.d, viewPortWidth, viewPortHeight);
		
		if(a != null && b != null) viewPort.drawLine(a, b, color);
		if(b != null && c != null) viewPort.drawLine(b, c, color);
		if(c != null && d != null) viewPort.drawLine(c, d, color);
		if(d != null && a != null) viewPort.drawLine(d, a, color);
	}
	
	static public void drawPoint(Bitmap viewPort, Camera camera, Vector3f point, int width) {
		Vector2i projectedPoint;
		
		int color = 0xFF00FF;
		
		int viewPortWidth = viewPort.width;
		int viewPortHeight = viewPort.height;
		
		projectedPoint = camera.projectCanvas(point, viewPortWidth, viewPortHeight);
		
		if(projectedPoint == null)
			return;
		
		Vector2i a = new Vector2i(projectedPoint.x - width, projectedPoint.y - width);
		Vector2i b = new Vector2i(projectedPoint.x + width, projectedPoint.y - width);
		Vector2i c = new Vector2i(projectedPoint.x + width, projectedPoint.y + width);
		Vector2i d = new Vector2i(projectedPoint.x - width, projectedPoint.y + width);
		
		if(projectedPoint != null) viewPort.drawRect(a, b, c, d, color);
	}
}
