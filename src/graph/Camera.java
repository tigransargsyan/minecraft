package graph;
import game.GameConfiguration;
import math.Matrix44f;
import math.Vector2f;
import math.Vector2i;
import math.Vector3f;

public class Camera {	
	/**
	 *  Camera orientation in WCS
	 */
	public Vector3f u, v, n;
	
	public double horizontalAngle;
	public double verticalAngle;
	
	private Matrix44f ccsToWcs;
	public Matrix44f wcsToCcs;
	
	/**
	 * Camera's position with respect to the World Coordinate System (WCS)
	 */
	public Vector3f position;
	
	private double canvasWidth;
	private double canvasHeight;
	private double hfov;
	private double vfov;
	private double top;
	private double right;
	private double bottom;
	private double left;
	
	public Camera(Vector3f position) {
		u = new Vector3f(1, 0, 0); // = x WCS
		v = new Vector3f(0, 0, 1); // = z WCS
		n = new Vector3f(0, -1, 0); // = -y WCS
		
		horizontalAngle = 0;
		verticalAngle = 0;
		
		canvasWidth = (GameConfiguration.FILM_APERTURE_WIDTH * GameConfiguration.INCH_TO_MM * GameConfiguration.Z_NEAR_CLIPPING) / GameConfiguration.FOCAL_LENGTH;
		canvasHeight = (GameConfiguration.FILM_APERTURE_HEIGHT * GameConfiguration.INCH_TO_MM * GameConfiguration.Z_NEAR_CLIPPING) / GameConfiguration.FOCAL_LENGTH;
		
		hfov = 2 * Math.atan(GameConfiguration.FILM_APERTURE_WIDTH * GameConfiguration.INCH_TO_MM * 2 / GameConfiguration.FOCAL_LENGTH);
		vfov = 2 * Math.atan(GameConfiguration.FILM_APERTURE_HEIGHT * GameConfiguration.INCH_TO_MM * 2 / GameConfiguration.FOCAL_LENGTH);
		
	    top = ((GameConfiguration.FILM_APERTURE_HEIGHT * GameConfiguration.INCH_TO_MM / 2) / GameConfiguration.FOCAL_LENGTH) * GameConfiguration.Z_NEAR_CLIPPING;
	    right = ((GameConfiguration.FILM_APERTURE_WIDTH * GameConfiguration.INCH_TO_MM / 2) / GameConfiguration.FOCAL_LENGTH) * GameConfiguration.Z_NEAR_CLIPPING;

	    bottom = -top;
	    left = -right;
	    
	    System.out.println("Canvas width: " + canvasWidth + " Canvas height: " + canvasHeight);
	    System.out.println("HFOV [deg]: " + hfov*180.0f/Math.PI + " VFOV [deg]: " + vfov*180.0f/Math.PI);
	    System.out.println("Top: " + top + " Left: " + left + " Bottom: " + bottom + " Right: " + right);
	    
	    ccsToWcs = new Matrix44f();
		wcsToCcs = new Matrix44f();
		
		/* Calculate ccsToWcs and wcsToCcs */
		setPosition(position);
	}
	
	/**
	 * Sets the camera position
	 * @param position Camera's position with respect to the World Coordinate System (WCS)
	 */
	public void setPosition(Vector3f position) {
		this.position = position;
		calculateCameraTransformationMatrix();
	}
	
	private void calculateCameraTransformationMatrix() {
		/* Calculate CCS to WCS matrix */
		ccsToWcs.A11 = u.x; 		ccsToWcs.A12 = u.y; 		ccsToWcs.A13 = u.z; 		ccsToWcs.A14 = 0;
		ccsToWcs.A21 = v.x; 		ccsToWcs.A22 = v.y; 		ccsToWcs.A23 = v.z; 		ccsToWcs.A24 = 0;
		ccsToWcs.A31 = n.x; 		ccsToWcs.A32 = n.y; 		ccsToWcs.A33 = n.z; 		ccsToWcs.A34 = 0;
		ccsToWcs.A41 = position.x;	ccsToWcs.A42 = position.y; 	ccsToWcs.A43 = position.z; 	ccsToWcs.A44 = 1;
		
		/* Calculate WCS to CCS matrix (inverse) */
		ccsToWcs.inverse(wcsToCcs);
	}
	
	/**
	 * Given a point expressed in the World Coordinate System (WCS), transforms
	 * it into Camera Coordinate System representation
	 * @param point
	 * @return
	 */
	private Vector3f transform(Vector3f point) {
		return wcsToCcs.multiply(point);
	}
	
	/**
	 * 
	 * @param point 3D point to be projected (must be expressed in Camera Coordinate System (CCS))
	 * @return The projection of the 2D point in the canvas (near clipping plane)
	 */
	private Vector2f project(Vector3f point) {
		Vector2f projectedPoint = new Vector2f();
		
		/* 
		 * The point z shall be smaller or equal to -Z_NEAR_CLIPPING
		 * In this way, we filter the antisymmetry projected points (in the plane +Z_NEAR_CLIPPING)!!
		 */
		if(point.z > (-1) * GameConfiguration.Z_NEAR_CLIPPING) return null;
		
		projectedPoint.x = (-1) * point.x * GameConfiguration.Z_NEAR_CLIPPING / point.z;
		projectedPoint.y = (-1) * point.y * GameConfiguration.Z_NEAR_CLIPPING / point.z;
		
		return projectedPoint;
	}
	
	public Vector2i projectCanvas(Vector3f point, int viewPortWidth, int viewPortHeight) {		
		Vector2f screenPoint = project(transform(point));
		
		if(screenPoint == null)
			return null;
		
		/*
		 * Normalize. Coordinates will be in the range [0,1]
		 */
		Vector2f normalizedPoint = new Vector2f(
				(screenPoint.x + right) / (2*right), 
				(screenPoint.y + top) / (2*top)
				);
		
		/* 
		 * If the x- or y-coordinate absolute value is greater than the canvas width
		 * or height respectively, the point is not visible
		 */
		if(screenPoint.x < left || screenPoint.x > right || screenPoint.y < bottom || screenPoint.y > top) return null;
		
		/*
		 * Finally convert to pixel coordinates. Don't forget to invert the y coordinate
		 */
		return new Vector2i((int)Math.floor(normalizedPoint.x * viewPortWidth), (int)Math.floor((1 - normalizedPoint.y) * viewPortHeight));
	}
		
	/**
	 * Rotate the vector base u, v, n by hAngle (v axis) and then vAngle (u axis)
	 * @param hAngle
	 * @param vAngle
	 */
	public void calcRotationMatrix(double hAngle, double vAngle) {
		double sH = Math.sin(hAngle);
		double sV = Math.sin(vAngle);
		double cH = Math.cos(hAngle);
		double cV = Math.cos(vAngle);
		
		Vector3f u1, v1, n1;
		
		u1 = new Vector3f(0, 0, 0);
		v1 = new Vector3f(0, 0, 0);
		n1 = new Vector3f(0, 0, 0);
		
		u1.x = cH * u.x - sH * n.x;
		u1.y = cH * u.y - sH * n.y;
		u1.z = cH * u.z - sH * n.z;
		
		v1.x = sV * sH * u.x + cV * v.x + sV * cH * n.x;
		v1.y = sV * sH * u.y + cV * v.y + sV * cH * n.y;
		v1.z = sV * sH * u.z + cV * v.z + sV * cH * n.z;
		
		n1.x = cV * sH * u.x - sV * v.x + cV * cH * n.x;
		n1.y = cV * sH * u.y - sV * v.y + cV * cH * n.y;
		n1.z = cV * sH * u.z - sV * v.z + cV * cH * n.z;
		
		u = u1;
		v = v1;
		n = n1;
		
		calculateCameraTransformationMatrix();
	}
	
	/**
	 * Rotate the vector base u, v, n by hAngle (v axis) and then vAngle (u axis)
	 * @param hAngle
	 * @param vAngle
	 */
	public void calcRotationMatrix2(double hAngle, double vAngle) {
		double sH = Math.sin(hAngle);
		double sV = Math.sin(vAngle);
		double cH = Math.cos(hAngle);
		double cV = Math.cos(vAngle);

		Vector3f u1, v1, n1;
		
		u1 = new Vector3f(0, 0, 0);
		v1 = new Vector3f(0, 0, 0);
		n1 = new Vector3f(0, 0, 0);
		
		u1.x = cH * u.x + sH * sV * v.x - sH * cV * n.x;
		u1.y = cH * u.y + sH * sV * v.y - sH * cV * n.y;
		u1.z = cH * u.z + sH * sV * v.z - sH * cV * n.z;
		
		v1.x = cV * v.x + sV * n.x;
		v1.y = cV * v.y + sV * n.y;
		v1.z = cV * v.z + sV * n.z;
		
		n1.x = sH * u.x - cH * sV * v.x + cH * cV * n.x;
		n1.y = sH * u.y - cH * sV * v.y + cH * cV * n.y;
		n1.z = sH * u.z - cH * sV * v.z + cH * cV * n.z;
		
		u = u1;
		v = v1;
		n = n1;
		
		calculateCameraTransformationMatrix();
	}
	
	/**
	 * Rotate the vector base u, v, n by hAngle (v axis) and then vAngle (u axis)
	 * @param hAngle
	 * @param vAngle
	 */
	public void calcRotationMatrix3(double deltaHorizontalAngle, double deltaVerticalAngle) {
		horizontalAngle += deltaHorizontalAngle;
		verticalAngle += deltaVerticalAngle;
		
		double sH = Math.sin(horizontalAngle);
		double sV = Math.sin(verticalAngle);
		double cH = Math.cos(horizontalAngle);
		double cV = Math.cos(verticalAngle);
		
		
		u = new Vector3f(1, 0, 0); // = x WCS
		v = new Vector3f(0, 0, 1); // = z WCS
		n = new Vector3f(0, -1, 0); // = -y WCS
		
		Vector3f u1, v1, n1;
		
		u1 = new Vector3f(0, 0, 0);
		v1 = new Vector3f(0, 0, 0);
		n1 = new Vector3f(0, 0, 0);
		
		u1.x = cH * u.x - sH * n.x;
		u1.y = cH * u.y - sH * n.y;
		u1.z = cH * u.z - sH * n.z;
		
		v1.x = sV * sH * u.x + cV * v.x + sV * cH * n.x;
		v1.y = sV * sH * u.y + cV * v.y + sV * cH * n.y;
		v1.z = sV * sH * u.z + cV * v.z + sV * cH * n.z;
		
		n1.x = cV * sH * u.x - sV * v.x + cV * cH * n.x;
		n1.y = cV * sH * u.y - sV * v.y + cV * cH * n.y;
		n1.z = cV * sH * u.z - sV * v.z + cV * cH * n.z;
		
		u = u1;
		v = v1;
		n = n1;
		
		calculateCameraTransformationMatrix();
	}
}
