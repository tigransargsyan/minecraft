package graph;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import game.Utils;
import game.World;
import game.cubes.Cube;
import game.cubes.Face;
import graph.texture.Bitmap;
import graph.texture.TextureLibrary;
import math.Vector2i;
import math.Vector3i;

public class Renderer {
	private Camera camera;
	private World world;
	private TextureLibrary textures;
	
	private Vector3i lastUpdatedPosition;
	private List<Face> visibleFaces;
	
	public Renderer(Camera camera, World world, TextureLibrary textures) {
		this.camera = camera;
		this.world = world;
		this.textures = textures;
	}
	
	public void render(Bitmap output) {
		for (Face face : getVisibleFaces()) {
			Vector2i textureIdx = face.textureIdx;
			DrawUtils.drawBitmap(output, camera, textures.getTexture(textureIdx.x, textureIdx.y), face.a, face.b, face.c, face.d);
		}
	}
	
	private void filterAndSortFaces() {
		
		/* Get the list of cubes which are considered to be near to the player (thus candidate to be displayed) */
		List<Cube> nearCubes = world.getNearCubeList1(Utils.convertPosition(camera.position), 20000);
		//List<Cube> nearCubes = getNearCubeList(Utils.convertPosition(player.camera.position), 30);
		
		visibleFaces = new LinkedList<Face>();
		
		/* Add all visible faces */
		for(Cube c : nearCubes) {
			for(Face f : c.getFaces()) {
				if(!f.isHidden && f.isVisible(camera.position))
					visibleFaces.add(f);
			}
		}
		
		/* Sort the visible faces according to the distance to the eye */
		Collections.sort(visibleFaces, new Comparator<Face>() {
			@Override
			public int compare(Face f1, Face f2) {
				double d1 = f1.distanceSquareFrom(camera.position);
				double d2 = f2.distanceSquareFrom(camera.position);
				return (d1 >= d2) ? -1 : 1;
			}
		});

	}
	
	private List<Face> getVisibleFaces() {
		Vector3i position = Utils.convertPosition(camera.position);
		
		if(position != lastUpdatedPosition || world.modified()) {
			world.markAsRefreshed();
			filterAndSortFaces();
		}
		return visibleFaces;
	}
}
