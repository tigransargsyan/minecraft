package graph;
import game.GameConfiguration;
import game.LookingAtCube;
import game.World;
import graph.texture.Bitmap;
import graph.texture.Font;
import graph.texture.TextureLibrary;
import math.Vector2i;
import math.Vector3f;

public class Screen extends Bitmap {
	private Bitmap viewPort;
	private Bitmap mapView;
	private Renderer worldRenderer;
	private Font font;
	private Camera camera;
	
	public LookingAtCube lookingAtCube;
	
	static int time = 0;
			
	public Screen(int width, int height, Camera camera, World world, TextureLibrary textures, Font font) {
		super(width, height);
		this.worldRenderer = new Renderer(camera, world, textures);
		this.font = font;
		this.camera = camera;
		
		viewPort = new Bitmap(width, height);
		mapView = new Bitmap(100, 100);		
	}

	public void render() {
		time++;
		
		/* Project the world into the viewPort */
		viewPort.clean();

		// Draw the faces
		worldRenderer.render(viewPort);
		
		/* Draw the "looking at cube" (if any) */
		drawLookingAtCube();
		
		if(GameConfiguration.getInstance().isAxisEnabled())
			drawAxis();

		drawInfo();
		
		/* Draw View port and the map view */
		draw(viewPort, 0, 0);
		//draw(mapView,0, 0);
	}
	
	private void drawLookingAtCube() {
		if(lookingAtCube != null) {
			if(lookingAtCube.cube != null) {
				DrawUtils.drawWireframeCube(viewPort, camera, lookingAtCube.cube);
			}
			
			if(lookingAtCube.face != null) {
				DrawUtils.drawWireframeFace(viewPort, camera, lookingAtCube.face);
			}
			
			if(lookingAtCube.intersectionPoint != null) {
				DrawUtils.drawPoint(viewPort, camera, lookingAtCube.intersectionPoint, (int) (Math.abs(1 / lookingAtCube.intersectionPointParameter)*20));
			}
		}
	}
	
	private void drawInfo() {
		int deltaY = 8;
		int startY = 0;
		font.renderText(new Vector2i(0,startY + 0*deltaY), "Axis: ".concat(GameConfiguration.getInstance().isAxisEnabled() ? "Yes" : "No"), viewPort);
		font.renderText(new Vector2i(0,startY + 1*deltaY), "Gravity: ".concat(GameConfiguration.getInstance().isGravityEnabled() ? "Yes" : "No"), viewPort);
		font.renderText(new Vector2i(0,startY + 2*deltaY), "Position = ".concat(camera.position.toString()), viewPort);
		font.renderText(new Vector2i(0,startY + 3*deltaY), "u = ".concat(camera.u.toString()), viewPort);
		font.renderText(new Vector2i(0,startY + 4*deltaY), "v = ".concat(camera.v.toString()), viewPort);
		font.renderText(new Vector2i(0,startY + 5*deltaY), "n = ".concat(camera.n.toString()), viewPort);
		font.renderText(new Vector2i(0,startY + 6*deltaY), "Angle(x,u) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(1, 0, 0), camera.u))), viewPort);
		font.renderText(new Vector2i(0,startY + 7*deltaY), "Angle(y,u) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(0, 1, 0), camera.u))), viewPort);
		font.renderText(new Vector2i(0,startY + 8*deltaY), "Angle(z,u) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(0, 0, 1), camera.u))), viewPort);
		
		font.renderText(new Vector2i(0,startY + 9*deltaY), "Angle(x,v) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(1, 0, 0), camera.v))), viewPort);
		font.renderText(new Vector2i(0,startY + 10*deltaY), "Angle(y,v) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(0, 1, 0), camera.v))), viewPort);
		font.renderText(new Vector2i(0,startY + 11*deltaY), "Angle(z,v) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(0, 0, 1), camera.v))), viewPort);
		
		font.renderText(new Vector2i(0,startY + 12*deltaY), "Angle(x,n) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(1, 0, 0), camera.n))), viewPort);
		font.renderText(new Vector2i(0,startY + 13*deltaY), "Angle(y,n) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(0, 1, 0), camera.n))), viewPort);
		font.renderText(new Vector2i(0,startY + 14*deltaY), "Angle(z,n) = ".concat(String.format("%.3f d", (180 / Math.PI) * Vector3f.angle(new Vector3f(0, 0, 1), camera.n))), viewPort);
		
		font.renderText(new Vector2i(0,startY + 15*deltaY), "H Angle = ".concat(String.format("%.3f d", (180 / Math.PI) * camera.horizontalAngle)), viewPort);
		font.renderText(new Vector2i(0,startY + 16*deltaY), "V Angle = ".concat(String.format("%.3f d", (180 / Math.PI) * camera.verticalAngle)), viewPort);
	}
	
	private void drawAxis() {
		/* ************* */
		/* Draw the axis */
		/* ************* */
		Bitmap axisView = viewPort;
		/* The axis will be drawn always in a distance of 10 from the camera (eye) */
		Vector3f o = camera.position.sum(camera.n.mult(-10));
		Vector2i origin = camera.projectCanvas(o, axisView.width, axisView.height);
		Vector2i xAxis = camera.projectCanvas(o.sum(new Vector3f(1, 0, 0)), axisView.width, axisView.height);
		Vector2i yAxis = camera.projectCanvas(o.sum(new Vector3f(0, 1, 0)), axisView.width, axisView.height);
		Vector2i zAxis = camera.projectCanvas(o.sum(new Vector3f(0, 0, 1)), axisView.width, axisView.height);
		Vector2i uAxis = camera.projectCanvas(o.sum(camera.u), axisView.width, axisView.height);
		Vector2i vAxis = camera.projectCanvas(o.sum(camera.v), axisView.width, axisView.height);
		Vector2i nAxis = camera.projectCanvas(o.sum(camera.n), axisView.width, axisView.height);
		
		int wcsAxisColor = 0xFFFF0000; /* RED color for WCS axis (alpha channel!) */
		int ccsAxisColor = 0xFFFFFF00; /* YELLOW color for CCS axis (alpha channel!) */
		
		if(origin != null) {
			if(xAxis != null) {
				axisView.drawLine(origin, xAxis, wcsAxisColor);
				font.renderText(xAxis, "x", axisView);
			}
			if(yAxis != null) {
				axisView.drawLine(origin, yAxis, wcsAxisColor);
				font.renderText(yAxis, "y", axisView);
			}
			if(zAxis != null) {
				axisView.drawLine(origin, zAxis, wcsAxisColor);
				font.renderText(zAxis, "z", axisView);
			}
			
			if(uAxis != null) {
				axisView.drawLine(origin, uAxis, ccsAxisColor);
				font.renderText(uAxis, "u", axisView);
			}
			if(vAxis != null) {
				axisView.drawLine(origin, vAxis, ccsAxisColor);
				font.renderText(vAxis, "v", axisView);
			}
			if(nAxis != null) {
				axisView.drawLine(origin, nAxis, ccsAxisColor);
				font.renderText(nAxis, "n", axisView);
			}
		}
	}
	
}
