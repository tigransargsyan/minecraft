package queue;

public interface QueueInterface<T> {
	/**
	 * Insert a new item onto queue
	 * @param item Item to be enqueued
	 */
	void enqueue(T item);
	
	/**
	 * Remove and return the item least recently added
	 * @return Least recently added item
	 */
	T dequeue();
	
	/**
	 * Checks whether the queue is empty or not
	 */
	boolean isEmpty();

}
