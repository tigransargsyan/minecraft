package queue;

public class LinkedQueue<T> implements QueueInterface<T> {

	/**
	 *   head    ---------  next  ---------    tail
	 *   ------> | Node1 | ------>| Node2 | <--------
	 *           ---------        ---------
	 */
	private class Node {
		T item;
		Node next;
	}
	
	private Node head, tail;
	
	public LinkedQueue() {
		head = null;
		tail = null;
	}
	
	@Override
	public void enqueue(T item) {
		Node newNode = new Node();
		newNode.item = item;
		newNode.next = null;
		
		if(isEmpty()) {
			head = newNode;
		} else {
			tail.next = newNode;
		}
		
		tail =  newNode;
	}

	@Override
	public T dequeue() {
		if(!isEmpty()) {
			T item = head.item;
			head = head.next;
			if(isEmpty()) {
				tail = null;
			}
			return item;
		}
		return null;
	}

	@Override
	public boolean isEmpty() {
		return head == null;
	}

}
