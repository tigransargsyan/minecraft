package game;
import java.awt.event.KeyEvent;

import game.cubes.Cube;
import game.cubes.GroundCube;
import graph.Screen;
import graph.texture.Font;
import graph.texture.TextureLibrary;
import math.Intersection;
import math.Ray;
import math.Vector2i;
import math.Vector3f;
import math.Vector3i;

public class Game {
	public World world;
	public GamePlayer player;
	public int time;
	public TextureLibrary textureLibrary;
	public Font font;
	public Screen screen;

	public Game(int width, int height) {
		time = 0;
		textureLibrary = new TextureLibrary(GameConfiguration.TEXTURE_DIMENSION, GameConfiguration.TEXTURE_DIMENSION, GameConfiguration.RESOURCE_PATH + GameConfiguration.TEXTURE_PACK);
		font = new Font(GameConfiguration.RESOURCE_PATH + "font.png");
		
		player = new GamePlayer(new Vector3f(5, 0, GameConfiguration.BLOCK_SIZE * 2)); /* TODO */
		
		world = new World();
		screen = new Screen(width, height, player.camera, world, textureLibrary, font);
	}
	
	void handleInput(boolean[] keys) {
		if (keys[KeyEvent.VK_LEFT]) player.camera.calcRotationMatrix3(0.01, 0);
		if (keys[KeyEvent.VK_RIGHT]) player.camera.calcRotationMatrix3(-0.01, 0);
		if (keys[KeyEvent.VK_UP]) player.camera.calcRotationMatrix3(0, 0.01);
		if (keys[KeyEvent.VK_DOWN]) player.camera.calcRotationMatrix3(0, -0.01);
		if (keys[KeyEvent.VK_W]) player.moveIn();
		if (keys[KeyEvent.VK_S]) player.moveOut();
		if (keys[KeyEvent.VK_A]) player.moveLeft();
		if (keys[KeyEvent.VK_D]) player.moveRight();
		
		if (keys[KeyEvent.VK_G]) {
			keys[KeyEvent.VK_G] = false;
			if(GameConfiguration.getInstance().isGravityEnabled()) GameConfiguration.getInstance().disableGravity();
			else GameConfiguration.getInstance().enableGravity();
		}
		
		if (keys[KeyEvent.VK_X]) {
			keys[KeyEvent.VK_X] = false;
			if(GameConfiguration.getInstance().isAxisEnabled()) GameConfiguration.getInstance().disableAxis();
			else GameConfiguration.getInstance().enableAxis();
		}
		
		if (keys[KeyEvent.VK_SPACE]) {
			keys[KeyEvent.VK_SPACE] = false;
			player.jump();
		}
	}
	
	void handlePhysics() {
		/* Physics */
		if(GameConfiguration.getInstance().isGravityEnabled()) {
			player.speed = player.speed.sum(new Vector3f(0, 0, GameConfiguration.GRAVITY));
			
			/* Current position */
			Vector3f currentPosition = player.position; 

			/* Predicted position */
			Vector3f nextPosition = currentPosition.sum(player.speed.mult(GameConfiguration.TIME_INTERVAL));
			int i1 = (int) (nextPosition.x / GameConfiguration.BLOCK_SIZE);
			int j1 = (int) (nextPosition.y / GameConfiguration.BLOCK_SIZE);
			int k1 = (int) (nextPosition.z / GameConfiguration.BLOCK_SIZE);
			
			/* Check for wrong position */
			if(nextPosition.z < GameConfiguration.EYE_HEIGHT)
				nextPosition.z = GameConfiguration.EYE_HEIGHT;
			
			Cube nextCube = world.getCube(new Vector3i(i1, j1, k1));
			if(nextCube == null) {
				currentPosition.set(nextPosition);
				player.camera.calcRotationMatrix(0, 0);
			} else {
				player.speed = new Vector3f(0, 0, 0);
			}
			

		}
	}
	
	public void tick(boolean[] keys) {
		time++;
		handleInput(keys);
		handlePhysics();

		/* Calculate the "looking at" cube */
		screen.lookingAtCube = calculateLookingAtCube(); // FIXME always? (update only where there is a need!)
	}
	
	
	public LookingAtCube calculateLookingAtCube() {
		LookingAtCube lookAtCube = new LookingAtCube();
		
		/* Find the nearest cube */
		Cube cube;
		for(double d = 1; d <= 5; d += 0.1) {
			Vector3f lookingAtPosition = player.position.sum(player.camera.n.mult((-1)*d*GameConfiguration.BLOCK_SIZE));
			Vector3i lookingAtCubePosition = new Vector3i(
					(int) ((lookingAtPosition.x+GameConfiguration.BLOCK_SIZE)/GameConfiguration.BLOCK_SIZE), /* Porque tengo que sumar BLOCK_SIZE?? */
					(int) ((lookingAtPosition.y+GameConfiguration.BLOCK_SIZE)/GameConfiguration.BLOCK_SIZE),
					(int) ((lookingAtPosition.z+GameConfiguration.BLOCK_SIZE)/GameConfiguration.BLOCK_SIZE));
			cube = world.getCube(lookingAtCubePosition);
			if(cube != null) {
				lookAtCube.cube = cube;
				break;
			}
		}
		if (lookAtCube.cube != null) {
			Ray ray = new Ray(player.position, player.camera.n.mult(-1.0));
			Intersection intersection = ray.intersect(lookAtCube.cube);
			if (intersection != null) {
				lookAtCube.intersectionPoint = intersection.hitPoint;
				lookAtCube.intersectionPointParameter = intersection.tHit;
				
				/* Figure out face intersection */
				lookAtCube.face = ray.intersectCubeFace(lookAtCube.cube);
			}
		}
		return lookAtCube;
	}
	
	
	public void handleMouseMovement(Vector2i current, Vector2i last) {
		if(last != null) {
			int dx = current.x - last.x;
			int dy = current.y - last.y;
			
			player.camera.calcRotationMatrix3(-dx * GameConfiguration.MOUSE_SENSITIVITY, -dy * GameConfiguration.MOUSE_SENSITIVITY);
			
			/* Calculate the "looking at" cube */
			screen.lookingAtCube = calculateLookingAtCube();
		}
	}
	
	public void handleLeftMouseClick(Vector2i current) {
		if(screen.lookingAtCube != null && screen.lookingAtCube.cube != null) {
			// Add a single cube
			if (screen.lookingAtCube.face != null) {
				world.createCube(new GroundCube(screen.lookingAtCube.cube.position.sum(screen.lookingAtCube.face.normal.toInteger())));
				world.performFaceHiding();
			}
		}
	}
	
	public void handleRightMouseClick(Vector2i current) {
		if(screen.lookingAtCube != null && screen.lookingAtCube.cube != null) {
			world.deleteCube(screen.lookingAtCube.cube.position);
			world.performFaceHiding();
		}
	}
}
