package game;

public class GameConfiguration {
	private static GameConfiguration mInstance = null;
	
	private boolean axis;
	private boolean gravity;
	
	public static final double BLOCK_SIZE = 1;
	public static final double TIME_INTERVAL = 0.05;
	public static final double GRAVITY = -0.6;
	public static final double LEGS_STRENGH = 8;
	public static final double EYE_HEIGHT = 2 * BLOCK_SIZE;
	public static final String RESOURCE_PATH = "/Users/tigransargsyan/desktop/minecraft/res/";
	public static final String TEXTURE_PACK = "textures_16.png";
	public static final int TEXTURE_DIMENSION = 16;
	public static final double MOUSE_SENSITIVITY = 0.001;
	// Camera related constants
	public static final double FILM_APERTURE_WIDTH = 0.825; /* [inch] */
	public static final double FILM_APERTURE_HEIGHT = 0.446; /* [inch] */
	public static final double FOCAL_LENGTH = 10; /* [mm] */
	public static final double Z_NEAR_CLIPPING = 0.1;
	public static final double Z_FAR_CLIPPING = 35;
	public static final double INCH_TO_MM = 25.4;

	public GameConfiguration() {
		axis = true;
		gravity = false;
	}
	
	public static GameConfiguration getInstance() {
		if(mInstance == null) mInstance = new GameConfiguration();
		return mInstance;
	}
	
	public boolean isAxisEnabled() {
		return axis;
	}
	
	public boolean isGravityEnabled() {
		return gravity;
	}
	
	public void enableAxis() {
		axis = true;
	}
	
	public void disableAxis() {
		axis = false;
	}
	
	public void enableGravity() {
		gravity = true;
	}
	
	public void disableGravity() {
		gravity = false;
	}
}
