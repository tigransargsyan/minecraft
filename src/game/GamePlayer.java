package game;

import graph.Camera;
import math.Vector3f;

public class GamePlayer {
	public Camera camera;
	public Vector3f position;
	public Vector3f speed;
	private static final double MOVE = 0.1;

	public GamePlayer(Vector3f position) {
		this.position = position;
		speed = new Vector3f(0, 0, 0);
		camera = new Camera(position);
	}

	public void moveUp() {
		position = position.sum(camera.v.mult(MOVE));
		updateCamera();
	}
	
	public void moveDown() {
		position = position.sum(camera.v.mult((-1)*MOVE));
		updateCamera();
	}
	
	public void moveLeft() {
		position = position.sum(camera.u.mult((-1)*MOVE));
		updateCamera();
	}
	
	public void moveRight() {
		position = position.sum(camera.u.mult(MOVE));
		updateCamera();
	}
	
	
	public void moveIn() {
		position = position.sum(camera.n.mult((-1)*MOVE));
		updateCamera();
	}
	
	public void moveOut() {
		position = position.sum(camera.n.mult(MOVE));
		updateCamera();
	}
	
	public void jump() {
		speed = new Vector3f(0, 0, GameConfiguration.LEGS_STRENGH);
	}
	
	public void updateCamera() {
		camera.setPosition(position);
	}
}
