package game;

import math.Vector3f;
import math.Vector3i;

public class Utils {
	public static Vector3i convertPosition(Vector3f position) {
		return new Vector3i((int) (position.x / GameConfiguration.BLOCK_SIZE),
				(int) (position.y / GameConfiguration.BLOCK_SIZE),
				(int) (position.z / GameConfiguration.BLOCK_SIZE));
	}
}
