package game;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import game.cubes.Cube;
import game.cubes.GroundCube;
import game.cubes.TreeBaseCube;
import game.cubes.TreeLeafsCube;
import math.Vector3i;
import queue.LinkedQueue;

public class World {
	private Map<Vector3i, Cube> world;
	private boolean isWorldModified;
	
	public World() {
		world = new HashMap<Vector3i, Cube>();
		createWorld();
		performFaceHiding();
		isWorldModified = true;
	}
	
	public boolean modified() {
		return isWorldModified;
	}
	
	public void markAsRefreshed() {
		isWorldModified = false;
	}
	
	private void createWorld() {
		for(int i = 0; i < 20; i++) {
			for(int j = 0; j < 20; j++) {
				createCube(new GroundCube(new Vector3i(i, j, 0)));
			}
		}
		createTree(5, 5, 1);
		createTree(10, 10, 1);
	}
	
	public void performFaceHiding() {//FIXME optimization, introduce full vs partial update, partial will get a hint about where the modification occured!
		for(Cube cube : world.values()) {
			cube.north.isHidden = false;
			cube.south.isHidden = false;
			cube.west.isHidden = false;
			cube.east.isHidden = false;
			cube.up.isHidden = false;
			cube.down.isHidden = false;
			if(getCube(cube.northPosition) != null) getCube(cube.position).north.isHidden = true;
			if(getCube(cube.southPosition) != null) getCube(cube.position).south.isHidden = true;
			if(getCube(cube.westPosition) != null) getCube(cube.position).west.isHidden = true;
			if(getCube(cube.eastPosition) != null) getCube(cube.position).east.isHidden = true;	
			if(getCube(cube.upPosition) != null) getCube(cube.position).up.isHidden = true;
			if(getCube(cube.downPosition) != null) getCube(cube.position).down.isHidden = true;	
		}
	}
	
	public void createTree(int i, int j, int k) {
		for(int height = k; height <= k+5; height++) {
			createCube(new TreeBaseCube(new Vector3i(i, j, height)));
		}
		
		int ii, jj, kk;
		Random random = new Random();
		
		int amount = 0;
		
		while(amount++ <= 50) {
			ii = -1 + random.nextInt() % 3;
			jj = -1 + random.nextInt() % 3;
			kk = -1 + random.nextInt() % 2;
			
			createCube(new TreeLeafsCube(new Vector3i(i + ii, j + jj, k + 5 + kk)));
		}
		isWorldModified = true;
	}

	public void createCube(Cube cube) {
		if(world.get(cube.position) == null) {
			world.put(cube.position, cube);
			isWorldModified = true;
		}
	}
	
	public void deleteCube(Vector3i position) {
		world.remove(position);
		isWorldModified = true;
	}
	
	public Cube getCube(Vector3i position) {
		return world.get(position);
	}
	

	public List<Cube> getNearCubeList(Vector3i position, int depth) {
		List<Cube> cubeList = new ArrayList<Cube>();
		Cube cube;
		
		for(int i = position.x - depth; i <= position.x + depth; i++) {
			for(int j = position.y - depth; j <= position.y + depth; j++) {
				for(int k = position.z - depth; k <= position.z + depth; k++) {
					if(i == position.x && j == position.y && k == position.z) {
						continue;
					} else {
						cube = getCube(new Vector3i(i, j, k));
						if(cube != null) {
							cubeList.add(cube);
						}
					}
				}
			}
		}
		
		return cubeList;
	}
	
	public List<Cube> getNearCubeList1(Vector3i position, int depth) {
		List<Cube> nearCubes = new LinkedList<Cube>();

		int i, j, k;
		
		i = position.x;
		j = position.y;
		k = position.z;

		/* The position has been moved, create the visible blocks (similar to BFS) */		
		LinkedQueue<Vector3i> queue = new LinkedQueue<Vector3i>();
		
		Vector3i p;
		Cube cube;
	
		if((i-1) >= 0) queue.enqueue(new Vector3i(i-1, j, k));
		if((i+1) < 50) queue.enqueue(new Vector3i(i+1, j, k));
		if((j-1) >= 0) queue.enqueue(new Vector3i(i, j-1, k));
		if((j+1) < 50) queue.enqueue(new Vector3i(i, j+1, k));
		if((k-1) >= 0) queue.enqueue(new Vector3i(i, j, k-1));
		if((k+1) < 50) queue.enqueue(new Vector3i(i, j, k+1));
		
		boolean[][][] visited = new boolean[50][50][50];
		visited[i][j][k] = true;
		// Review this algorithm
		int distance = 1;
		while(!queue.isEmpty()) {
			if(distance > depth) break;
			else {
				distance++;
			}
			p = queue.dequeue();
			cube = getCube(p);
			visited[p.x][p.y][p.z] = true;
			
			if(cube != null) {
				nearCubes.add(cube);
			} else {
				if((p.x-1) >= 0) {
					if(!visited[p.x-1][p.y][p.z]) {
						queue.enqueue(new Vector3i(p.x-1, p.y, p.z));
						visited[p.x-1][p.y][p.z] = true;
					}
				}
				
				if((p.x+1) < 50) {
					if(!visited[p.x+1][p.y][p.z]) {
						queue.enqueue(new Vector3i(p.x+1, p.y, p.z));
						visited[p.x+1][p.y][p.z] = true;
					}
				}
				
				if((p.y-1) >= 0) {
					if(!visited[p.x][p.y-1][p.z]) {
						queue.enqueue(new Vector3i(p.x, p.y-1, p.z));
						visited[p.x][p.y-1][p.z] = true;
					}
				}
				
				if((p.y+1) < 50) {
					if(!visited[p.x][p.y+1][p.z]) {
						queue.enqueue(new Vector3i(p.x, p.y+1, p.z));
						visited[p.x][p.y+1][p.z] = true;
					}
				}
				
				if((p.z-1) >= 0) {
					if(!visited[p.x][p.y][p.z-1]) {
						queue.enqueue(new Vector3i(p.x, p.y, p.z-1));
						visited[p.x][p.y][p.z-1] = true;
					}
				}
				
				if((p.z+1) < 50) {
					if(!visited[p.x][p.y][p.z+1]) {
						queue.enqueue(new Vector3i(p.x, p.y, p.z+1));
						visited[p.x][p.y][p.z+1] = true;
					}
				}
			}
		}
		return nearCubes;
	}
}
