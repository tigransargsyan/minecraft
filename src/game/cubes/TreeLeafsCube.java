package game.cubes;

import math.Vector2i;
import math.Vector3i;

public class TreeLeafsCube extends Cube {

	public TreeLeafsCube(Vector3i position) {
		super(position,
				new Vector2i(4, 3),
				new Vector2i(4, 3),
				new Vector2i(4, 3),
				new Vector2i(4, 3),
				new Vector2i(4, 3),
				new Vector2i(4, 3));
	}
}
