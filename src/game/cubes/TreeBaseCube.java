package game.cubes;

import math.Vector2i;
import math.Vector3i;

public class TreeBaseCube extends Cube {

	public TreeBaseCube(Vector3i position) {
		super(position,
				new Vector2i(5, 1),
				new Vector2i(5, 1),
				new Vector2i(4, 1),
				new Vector2i(4, 1),
				new Vector2i(4, 1),
				new Vector2i(4, 1));
	}
}
