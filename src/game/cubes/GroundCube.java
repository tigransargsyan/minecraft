package game.cubes;

import math.Vector2i;
import math.Vector3i;

public class GroundCube extends Cube {

	public GroundCube(Vector3i position) {
		super(position,
				new Vector2i(0, 0),
				new Vector2i(2, 0),
				new Vector2i(3, 0),
				new Vector2i(3, 0),
				new Vector2i(3, 0),
				new Vector2i(3, 0));
	}
}
