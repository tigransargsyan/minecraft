package game.cubes;

import java.util.ArrayList;

import game.GameConfiguration;
import math.Vector2i;
import math.Vector3f;
import math.Vector3i;

/**   
 *         (+)
 *          z
 *          |     (+)
 *                y
 *         (Up)  / 
 *          |
 *          | (N)
 *          | /
 *          |/
 *  (W)-----/------(E) ---- x (+)
 *         /|
 *        / |
 *      (S) |
 *          |
 *        (Down)
 * 
 *  z  (f)--------------(g)
 *  |  /|               /|
 *  | / |              / |
 *   /  |             /  |
 * (b)--------------(c)  |
 *  |   |            |   |
 *  |  (e)-----------|--(h)
 *  |  /             |  /
 *  | /              | /
 *  |/               |/
 * (a)--------------(d)  ---> x
 * 
 * @author tigran
 * 
 */
public class Cube {
	public Vector3f center;
	public Vector3f dimensions;

	public Vector3f a, b, c, d, e, f, g, h;
	
	public Face up;
	public Face down;
	public Face north;
	public Face south;
	public Face west;
	public Face east;
	
	public ArrayList<Face> faceList;
	
	public Vector3i position;
	public Vector3i northPosition;
	public Vector3i southPosition;
	public Vector3i westPosition;
	public Vector3i eastPosition;
	public Vector3i upPosition;
	public Vector3i downPosition;
	
	public Cube(Vector3i position,
			Vector2i upFaceTexture, Vector2i downFaceTexture,
			Vector2i northFaceTexture, Vector2i southFaceTexture,
			Vector2i eastFaceTexture, Vector2i westFaceTexture) {
		this(
				(position.x - 1) * GameConfiguration.BLOCK_SIZE,
				(position.y - 1) * GameConfiguration.BLOCK_SIZE,
				(position.z - 1) * GameConfiguration.BLOCK_SIZE,
				GameConfiguration.BLOCK_SIZE,
				GameConfiguration.BLOCK_SIZE,
				GameConfiguration.BLOCK_SIZE,
				upFaceTexture, downFaceTexture,
				northFaceTexture, southFaceTexture,
				eastFaceTexture, westFaceTexture);
		this.position = position;
		this.northPosition = new Vector3i(position.x, position.y + 1, position.z);
		this.southPosition = new Vector3i(position.x, position.y - 1, position.z);
		this.westPosition = new Vector3i(position.x - 1, position.y, position.z);
		this.eastPosition = new Vector3i(position.x + 1, position.y, position.z);
		this.upPosition = new Vector3i(position.x, position.y, position.z + 1);
		this.downPosition = new Vector3i(position.x, position.y, position.z - 1);
	}
	
	private Cube(double x, double y, double z, double width, double height, double deep,
			Vector2i upFaceTexture, Vector2i downFaceTexture,
			Vector2i northFaceTexture, Vector2i southFaceTexture,
			Vector2i eastFaceTexture, Vector2i westFaceTexture) {
		
		center = new Vector3f(x, y, z);
		dimensions = new Vector3f(width, deep, height);
		
		a = new Vector3f(x, y, z);
		b = new Vector3f(x, y, z + height);
		c = new Vector3f(x + width, y, z + height);
		d = new Vector3f(x + width, y, z);
		
		e = new Vector3f(x, y + deep, z);
		f = new Vector3f(x, y + deep, z + height);
		g = new Vector3f(x + width, y + deep, z + height);
		h = new Vector3f(x + width, y + deep, z);
		
		up = new Face(f, b, c, g, upFaceTexture, new Vector3f(0, 0, 1));
		down = new Face(a, e, h, d, downFaceTexture, new Vector3f(0, 0, -1));
		
		north = new Face(g, h, e, f, northFaceTexture, new Vector3f(0, 1, 0));
		south = new Face(b, a, d, c, southFaceTexture, new Vector3f(0, -1, 0));
		
		east = new Face(c, d, h, g, eastFaceTexture, new Vector3f(1, 0, 0));
		west = new Face(f, e, a, b, westFaceTexture, new Vector3f(-1, 0, 0));
		
		faceList = new ArrayList<Face>();
		faceList.add(up);
		faceList.add(down);
		faceList.add(north);
		faceList.add(west);
		faceList.add(east);
		faceList.add(south);
	}
	
	public ArrayList<Face> getFaces() {
		return faceList;
	}
	
	@Override
	public String toString() {
		return position.toString();
	}
}
