package game.cubes;
import math.Vector2i;
import math.Vector3f;

/** 
 * 
 * (a)--------------(d)
 *  |                |
 *  |        +       |
 *  |       /        |
 *  |      /         |
 *  |    (n)         |
 * (b)--------------(c)
 * 
 * The order of a-b-c-d is important for texture rendering!
 * 
 * @author tigran
 * 
 */
public class Face {
	public Vector3f a, b, c, d;
	public Vector3f ab, ad;
	public Vector3f center;
	public Vector2i textureIdx;
	public boolean isHidden;
	public Vector3f normal;
	
	public Face(Vector3f a, Vector3f b, Vector3f c, Vector3f d, Vector2i textureIdx, Vector3f normal) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.ab = b.sub(a);
		this.ad = d.sub(a);
		this.center = a.sum(c).mult(0.5);
		this.normal = normal;
		this.textureIdx = textureIdx;
		isHidden = false;
	}
	
	/**
	 * Tests for invisible faces
	 * @param eyePosition
	 * @return
	 */
	public boolean isVisible(Vector3f eyePosition) {
		return Vector3f.dotProduct(a.sum(eyePosition.mult(-1)), normal) <= 0; /* TODO entender mejor */
	}
	
	/**
	 * Returns the distance square from the face center
	 * @param point
	 * @return
	 */
	public double distanceSquareFrom(Vector3f point) {
		return point.sum(center.mult(-1)).moduleSquare();
	}
}
