package game;
import java.awt.AWTException;
import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import graph.Screen;
import math.Vector2i;

public class GameWindow extends Canvas implements Runnable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Minecraft window size: 850 x 480 (scale 1?)
	 */
	private static final int WIDTH = 850;
	private static final int HEIGHT = 480;
	private static final int SCALE = 1;
	
	private JFrame frame;
	Robot robot;
	
	private boolean running = false;
	private Thread thread;
	
	private Game game;
	
	private BufferedImage img;
	private int[] pixels;
	
	InputHandler keyListener;
	
	Vector2i mouseLastPosition;
	
	
	private int frames;
	private long currentTime;
	
	
	public GameWindow(JFrame frame){
		Dimension size = new Dimension(WIDTH*SCALE, HEIGHT*SCALE);
		
		this.frame = frame;
		
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
		
		/* For FPS */
		frames = 0;
		currentTime = getSeconds();
		
		game = new Game(WIDTH, HEIGHT);
				
		img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB); /* TODO */
		
		pixels = ((DataBufferInt)img.getRaster().getDataBuffer()).getData();
		
		keyListener = new InputHandler();
		addKeyListener(keyListener);
		
		/* For mouse */
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent event) {
				Vector2i mousePosition = new Vector2i(event.getX(), event.getY());
				game.handleMouseMovement(mousePosition, mouseLastPosition);
				mouseLastPosition = mousePosition;
				centerMouse(event);
			}
			
			@Override
			public void mouseDragged(MouseEvent arg0) {}
		});
		
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {}
			
			@Override
			public void mousePressed(MouseEvent event) {
				Vector2i mousePosition = new Vector2i(event.getX(), event.getY());
				if(SwingUtilities.isLeftMouseButton(event)) {
					game.handleLeftMouseClick(mousePosition);
				} else if(SwingUtilities.isRightMouseButton(event)) {
					game.handleRightMouseClick(mousePosition);
				}
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) { }
			
			@Override
			public void mouseEntered(MouseEvent arg0) { }
			
			@Override
			public void mouseClicked(MouseEvent arg0) { }
		});
	}
	
	public static void main(String[] args){
		JFrame frame = new JFrame("Game");
		GameWindow gameWindow = new GameWindow(frame);
		
		frame.add(gameWindow);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setCursor(frame);
		gameWindow.start();
	}
	
	private void centerMouse(MouseEvent event) {
		/* Center the mouse pointer */
		Point locationOnScreen = frame.getLocationOnScreen();
		if(event.getX() > WIDTH*SCALE/2 || event.getY() > HEIGHT*SCALE/2 || event.getX() < WIDTH*SCALE/4 || event.getY() < HEIGHT*SCALE/4) { /* TODO */
			robot.mouseMove(locationOnScreen.x + (frame.getWidth()/2), locationOnScreen.y + (frame.getHeight()/2));
			mouseLastPosition = null;
		}
	}
	
	private static void setCursor(JFrame frame) {
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		int[] cursorPixels = ((DataBufferInt)cursorImg.getRaster().getDataBuffer()).getData();
		
		/* Draw cursor */
		for(int i = 6; i <= 8; i++) {
			for(int j = 0; j < 16; j++) {
				cursorPixels[i + j * 16] = 0xFFFFFF00;
			}
		}
		for(int i = 0; i < 16; i++) {
			for(int j = 6; j <= 8; j++) {
				cursorPixels[i + j * 16] = 0xFFFFFF00;
			}
		}
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
		frame.getContentPane().setCursor(blankCursor);
	}
	
	private synchronized void start() {
		if(running) return;
		running = true;
		thread = new Thread(this);
		
		thread.start(); // Run run method of the current class
	}
	
	private synchronized void stop() {
		if(!running)
			return;
		running = false;		
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void run(){
		while(running){
			tick();
			render();
		}
	}
	
	private void render() {
		game.screen.render();
		
		// Copy from screen.pixels to pixels
		for(int i=0; i<WIDTH*HEIGHT; i++){
			pixels[i] = game.screen.pixels[i];
//			pixelsAlpha[i] = (screen.pixels[i] & 0xFF000000) >> 24;
		}
		
		BufferStrategy bufferStrategy = this.getBufferStrategy();
		if(bufferStrategy == null){
			this.createBufferStrategy(2);
			return;
		}
		
		Graphics gr = bufferStrategy.getDrawGraphics();
		gr.drawImage(img, 0, 0, WIDTH*SCALE, HEIGHT*SCALE, null);
		gr.dispose();
		bufferStrategy.show();
		
		/* FPS */
		frames++;
		if(currentTime != getSeconds()) {
			frame.setTitle("FPS: " + frames);
			frames = 0;
			currentTime = getSeconds();
		}
		
	}
	private void tick() {
		game.tick(keyListener.keys);
	}
	
	private long getSeconds() {
		return (long)Math.floor(System.nanoTime() / 1E9);
	}

}
