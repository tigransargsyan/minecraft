package game;
import game.cubes.Cube;
import game.cubes.Face;
import math.Vector3f;

public class LookingAtCube {
	public Cube cube;
	public Face face;
	public Vector3f intersectionPoint;
	public double intersectionPointParameter;	
}
