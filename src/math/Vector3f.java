package math;

public class Vector3f {
	public Vector3f(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public Vector3f() {
		this.x = 0.0;
		this.y = 0.0;
		this.z = 0.0;
	}
	
	public Vector3f mult(double k) {
		return new Vector3f(x*k, y*k, z*k);
	}
	
	public Vector3f sum(Vector3f v) {
		return new Vector3f(x + v.x, y + v.y, z + v.z);
	}
	
	public Vector3f sub(Vector3f v) {
		return new Vector3f(x - v.x, y - v.y, z - v.z);
	}
	
	public static double dotProduct(Vector3f a, Vector3f b) {
		return a.x*b.x + a.y*b.y + a.z*b.z;
	}
	
	/**
	 * 
	 * a x b
	 * 
	 * | ax |   | bx |   | ay * bz - ax * by |
	 * | ay | x | by | = | -bz * ax + bx * ax |
	 * | az |   | bz |   | ax * by - bx * ay |
	 */
	public static Vector3f crossProduct(Vector3f a, Vector3f b) {
		return new Vector3f(
				a.y * b.z - a.x*b.y,
				-b.z * a.x + b.x * a.x,
				a.x * b.y - b.x * a.y
				);
	}
		
	public double x;
	public double y;
	public double z;
	
	public double module() {
		return Math.sqrt(x*x+y*y+z*z);
	}
	
	public double moduleSquare() {
		return x*x + y*y + z*z;
	}
	
	public String toString() {
		return "[".concat(String.format("%.3f", x)).concat(", ").concat(String.format("%.3f", y)).concat(", ").concat(String.format("%.3f", z)).concat("]");
	}
	
	public static double angle(Vector3f a, Vector3f b) {
		return Math.acos(dotProduct(a,b)/ (a.module()*b.module()));
	}
	
	public void set(Vector3f v) {
		this.x = v.x;
		this.y = v.y;
		this.z = v.z;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Vector3f))
			return false;
		Vector3f c = (Vector3f) obj;
		return c.x == x && c.y == y && c.z == z;
	}
	
	public Vector3i toInteger() {
		return new Vector3i((int)x, (int)y, (int)z);
	}
}
