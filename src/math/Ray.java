package math;

import game.cubes.Cube;
import game.cubes.Face;

public class Ray {
	private Vector3f origin, direction, invDirection;
	private int[] sign;
	
	public Ray(Vector3f origin, Vector3f direction) {
		this.origin = origin;
		this.direction = direction;

		invDirection = new Vector3f(1 / direction.x, 1 / direction.y, 1 / direction.z);
		sign = new int[3];
		sign[0] = (invDirection.x < 0) ? 1 : 0;
		sign[1] = (invDirection.y < 0) ? 1 : 0;
		sign[2] = (invDirection.z < 0) ? 1 : 0;
	}
	
	public Intersection intersect(Cube cube) {
		Vector3f[] bounds = {cube.a, cube.g};
		double tmin, tmax, tymin, tymax, tzmin, tzmax;
		tmin = (bounds[sign[0]].x - origin.x) * invDirection.x;
		tmax = (bounds[1 - sign[0]].x - origin.x) * invDirection.x;
		tymin = (bounds[sign[1]].y - origin.y) * invDirection.y;
		tymax = (bounds[1 - sign[1]].y - origin.y) * invDirection.y;
		if ((tmin > tymax) || (tymin > tmax))
			return null;
		if (tymin > tmin)
			tmin = tymin;
		if (tymax < tmax)
			tmax = tymax;
		tzmin = (bounds[sign[2]].z - origin.z) * invDirection.z;
		tzmax = (bounds[1 - sign[2]].z - origin.z) * invDirection.z;
		if ( (tmin > tzmax) || (tzmin > tmax) )
			return null;
		if (tzmin > tmin)
			tmin = tzmin;
		if (tzmax < tmax)
			tmax = tzmax;
	    if (tmin < 0 && tmax < 0) /* No intersection as both are negatives */
	    	return null;
		double t = Math.min(tmin, tmax); /* Find smallest positive */
		return new Intersection(t, origin.sum(direction.mult(t)));
	}
	
	public Intersection intersect(Face face) {
		Vector3f normal = face.normal.mult(-1);
		/* First calculate ray plane intersection */
		double denom = Vector3f.dotProduct(normal, direction);
		double t;
		if (denom > 1E-6) {
			Vector3f p010 = face.center.sub(origin);
			t = Vector3f.dotProduct(p010, normal) / denom;
		} else
			return null;
		if (t < 0)
			return null;
		
		/* Calculate the hit point */
		Vector3f p = origin.sum(direction.mult(t));
		
		/* Check if hit point is inside the face */
		// (0 < AP⋅AB < AB⋅AB ) && (0 < AP⋅AD < AD⋅AD)
		Vector3f ap = p.sub(face.a);
		double abab = Vector3f.dotProduct(face.ab, face.ab);
		double adad = Vector3f.dotProduct(face.ad, face.ad);
		double apab = Vector3f.dotProduct(ap, face.ab);
		double apad = Vector3f.dotProduct(ap, face.ad);
		if (0 < apab && apab < abab && 0 < apad && apad < adad)  {
			return new Intersection(t, p);
		}
		return null;
	}
	
	public Face intersectCubeFace(Cube cube) {
		Face foundFace = null;
		double tmin = Double.POSITIVE_INFINITY;
		Intersection i;
		
		Face[] faces = {cube.west, cube.east, cube.north, cube.south, cube.up, cube.down};
		
		for (Face face : faces) {
			i = intersect(face);
			if (i != null && i.tHit >= 0 && i.tHit < tmin) { // FIXME the algorithm intersect always returns tHit > 0?
				tmin = i.tHit;
				foundFace = face;
			}
		}
		return foundFace;
	}
}
