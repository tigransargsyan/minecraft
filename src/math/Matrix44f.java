package math;

public class Matrix44f {
	public double A11, A12, A13, A14;
	public double A21, A22, A23, A24;
	public double A31, A32, A33, A34;
	public double A41, A42, A43, A44;
	
	public Matrix44f() {
	}
	
	/**
	 * Performs v' = v' * M 
	 *                            |A11 A12 A13 A14|
	 *                            |A21 A22 A23 A24|
	 * [x' y' y' w'] = [x y z w] *|A31 A32 A33 A34|
	 *                            |A41 A42 A43 A44|
	 *                            
	 * x' = x * A11 + y * A21 + z * A31 + w * A41
	 * ...
	 *                            
	 * @param vector
	 * @return
	 */
	public Vector4f multiply(Vector4f v) {
		Vector4f result = new Vector4f();
		
		result.x = v.x * A11 + v.y * A21 + v.z * A31 + v.w * A41;
		result.y = v.x * A12 + v.y * A22 + v.z * A32 + v.w * A42;
		result.z = v.x * A13 + v.y * A23 + v.z * A33 + v.w * A43;
		result.w = v.x * A14 + v.y * A24 + v.z * A34 + v.w * A44;
		
		return result;
	}
	
	/**
	 * Performs v' = v' * M 
	 *                         |A11 A12 A13 0|
	 *                         |A21 A22 A23 0|
	 * [x' y' y'] = [x y z 1] *|A31 A32 A33 0|
	 *                         |A41 A42 A43 1|
	 *                            
	 * x' = x * A11 + y * A21 + z * A31 + A41
	 * ...
	 *                            
	 * @param vector
	 * @return
	 */
	public Vector3f multiply(Vector3f v) {
		Vector3f result = new Vector3f();
		
		result.x = v.x * A11 + v.y * A21 + v.z * A31 + A41;
		result.y = v.x * A12 + v.y * A22 + v.z * A32 + A42;
		result.z = v.x * A13 + v.y * A23 + v.z * A33 + A43;
		
		return result;
	}
	
	/**
	 * 
	 * Performs M * M1
	 * 
	 * |A11 A12 A13 A14|   |A11 A12 A13 A14|
	 * |A21 A22 A23 A24|   |A21 A22 A23 A24|
	 * |A31 A32 A33 A34| * |A31 A32 A33 A34|
	 * |A41 A42 A43 A44|   |A41 A42 A43 A44|
	 * 
	 * @param m
	 * @return
	 */
	public Matrix44f multiply(Matrix44f n) {
		Matrix44f result = new Matrix44f();

		result.A11 = A11 * n.A11 + A12 * n.A21 + A13 * n.A31 + A14 * n.A41;
		result.A12 = A11 * n.A12 + A12 * n.A22 + A13 * n.A32 + A14 * n.A42;
		result.A13 = A11 * n.A13 + A12 * n.A23 + A13 * n.A33 + A14 * n.A43;
		result.A14 = A11 * n.A14 + A12 * n.A24 + A13 * n.A34 + A14 * n.A44;
		
		result.A21 = A21 * n.A11 + A22 * n.A21 + A23 * n.A31 + A24 * n.A41;
		result.A22 = A21 * n.A12 + A22 * n.A22 + A23 * n.A32 + A24 * n.A42;
		result.A23 = A21 * n.A13 + A22 * n.A23 + A23 * n.A33 + A24 * n.A43;
		result.A24 = A21 * n.A14 + A22 * n.A24 + A23 * n.A34 + A24 * n.A44;
		
		result.A31 = A31 * n.A11 + A32 * n.A21 + A33 * n.A31 + A34 * n.A41;
		result.A32 = A31 * n.A12 + A32 * n.A22 + A33 * n.A32 + A34 * n.A42;
		result.A33 = A31 * n.A13 + A32 * n.A23 + A33 * n.A33 + A34 * n.A43;
		result.A34 = A31 * n.A14 + A32 * n.A24 + A33 * n.A34 + A34 * n.A44;
		
		result.A41 = A41 * n.A11 + A42 * n.A21 + A43 * n.A31 + A44 * n.A41;
		result.A42 = A41 * n.A12 + A42 * n.A22 + A43 * n.A32 + A44 * n.A42;
		result.A43 = A41 * n.A13 + A42 * n.A23 + A43 * n.A33 + A44 * n.A43;
		result.A44 = A41 * n.A14 + A42 * n.A24 + A43 * n.A34 + A44 * n.A44;
		
		return result;
	}
	
	public double det() {
		return	A11 * A22 * A33 * A44 + A11 * A23 * A34 * A42 + A11 * A24 * A32 * A43 +
				A12 * A21 * A34 * A43 + A12 * A23 * A31 * A44 + A12 * A24 * A33 * A41 +
				A13 * A21 * A32 * A44 + A13 * A22 * A34 * A41 + A13 * A24 * A31 * A42 +
				A14 * A21 * A33 * A42 + A14 * A22 * A31 * A43 + A14 * A23 * A32 * A41 -
				A11 * A22 * A34 * A43 - A11 * A23 * A32 * A44 - A11 * A24 * A33 * A42 -
				A12 * A21 * A33 * A44 - A12 * A23 * A34 * A41 - A12 * A24 * A31 * A43 -
				A13 * A21 * A34 * A42 - A13 * A22 * A31 * A44 - A13 * A24 * A32 * A41 -
				A14 * A21 * A32 * A43 - A14 * A22 * A33 * A41 - A14 * A23 * A31 * A42;
	}
	
	public void inverse(Matrix44f out) {
		double det = det();
		
		out.A11 = A22 * A33 * A44 + A23 * A34 * A42 + A24 * A32 * A43 - A22 * A34 * A43 - A23 * A32 * A44 - A24 * A33 * A42;
		out.A12 = A12 * A34 * A43 + A13 * A32 * A44 + A14 * A33 * A42 - A12 * A33 * A44 - A13 * A34 * A42 - A14 * A32 * A43;
		out.A13 = A12 * A23 * A44 + A13 * A24 * A42 + A14 * A22 * A43 - A12 * A24 * A43 - A13 * A22 * A44 - A14 * A23 * A42;
		out.A14 = A12 * A24 * A33 + A13 * A22 * A34 + A14 * A23 * A32 - A12 * A23 * A34 - A13 * A24 * A32 - A14 * A22 * A33;
		out.A21 = A21 * A34 * A43 + A23 * A31 * A44 + A24 * A33 * A41 - A21 * A33 * A44 - A23 * A34 * A41 - A24 * A31 * A43;
		out.A22 = A11 * A33 * A44 + A13 * A34 * A41 + A14 * A31 * A43 - A11 * A34 * A43 - A13 * A31 * A44 - A14 * A33 * A41;
		out.A23 = A11 * A24 * A43 + A13 * A21 * A44 + A14 * A23 * A41 - A11 * A23 * A44 - A13 * A24 * A41 - A14 * A21 * A43;
		out.A24 = A11 * A23 * A34 + A13 * A24 * A31 + A14 * A21 * A33 - A11 * A24 * A33 - A13 * A21 * A34 - A14 * A23 * A31;
		out.A31 = A21 * A32 * A44 + A22 * A34 * A41 + A24 * A31 * A42 - A21 * A34 * A42 - A22 * A31 * A44 - A24 * A32 * A41;
		out.A32 = A11 * A34 * A42 + A12 * A31 * A44 + A14 * A32 * A41 - A11 * A32 * A44 - A12 * A34 * A41 - A14 * A31 * A32;
		out.A33 = A11 * A22 * A44 + A12 * A24 * A41 + A14 * A21 * A42 - A11 * A24 * A42 - A12 * A21 * A44 - A14 * A22 * A41;
		out.A34 = A11 * A24 * A32 + A12 * A21 * A34 + A14 * A22 * A31 - A11 * A22 * A34 - A12 * A24 * A31 - A14 * A21 * A32;
		out.A41 = A21 * A33 * A42 + A22 * A31 * A43 + A23 * A32 * A41 - A21 * A32 * A43 - A22 * A33 * A41 - A23 * A31 * A42;
		out.A42 = A11 * A32 * A43 + A12 * A33 * A41 + A13 * A31 * A42 - A11 * A33 * A42 - A12 * A31 * A43 - A13 * A32 * A41;
		out.A43 = A11 * A23 * A42 + A12 * A21 * A43 + A13 * A22 * A41 - A11 * A22 * A43 - A12 * A23 * A41 - A13 * A21 * A42;
		out.A44 = A11 * A22 * A33 + A12 * A23 * A31 + A13 * A21 * A32 - A11 * A23 * A32 - A12 * A21 * A33 - A13 * A22 * A31;

		out.A11 /= det;
		out.A12 /= det;
		out.A13 /= det;
		out.A14 /= det;
		out.A21 /= det;
		out.A22 /= det;
		out.A23 /= det;
		out.A24 /= det;
		out.A31 /= det;
		out.A32 /= det;
		out.A33 /= det;
		out.A34 /= det;
		out.A41 /= det;
		out.A42 /= det;
		out.A43 /= det;
		out.A44 /= det;
	}
	
	public String toString() {
		return	A11 + " " + A12 + " " + A13 + " " + A14 + "\n" +
				A21 + " " + A22 + " " + A23 + " " + A24 + "\n" +
				A31 + " " + A32 + " " + A33 + " " + A34 + "\n" +
				A41 + " " + A42 + " " + A43 + " " + A44;
	}
}
