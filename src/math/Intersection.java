package math;

public class Intersection {
	public double tHit;
	public Vector3f hitPoint;
	
	public Intersection(double tHit, Vector3f hitPoint) {
		super();
		this.tHit = tHit;
		this.hitPoint = hitPoint;
	}
	
	@Override
	public String toString() {
		return String.valueOf(tHit) + " " + hitPoint;
	}
}
