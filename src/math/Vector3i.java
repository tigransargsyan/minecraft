package math;

public class Vector3i {
	public Vector3i(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public Vector3i() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	public int x;
	public int y;
	public int z;
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Vector3i))
			return false;
		Vector3i c = (Vector3i) obj;
		return c.x == x && c.y == y && c.z == z;
	}
	
	@Override
	public int hashCode() {
		int hash = 17;
		hash = 31 * hash + x;
		hash = 31 * hash + y;
		hash = 31 * hash + z;
		return hash;
	}
	
	@Override
	public String toString() {
		return "[" + x + ", " + y + ", " + z + "]";
	}
	
	public Vector3i sum(Vector3i v) {
		return new Vector3i(x + v.x, y + v.y, z + v.z);
	}
}
