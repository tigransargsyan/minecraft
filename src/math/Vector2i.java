package math;

public class Vector2i {
	public Vector2i(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2i() {
		this.x = 0;
		this.y = 0;
	}
	
	public static double crossProduct(Vector2i a, Vector2i b) {
		return a.x*b.y - b.x*a.y;
	}

	public int x;
	public int y;
}
