package math;

public class Vector4f {
	public Vector4f(double x, double y, double z, double w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	public Vector4f() {
		this.x = 0.0;
		this.y = 0.0;
		this.z = 0.0;
		this.w = 0.0;
	}
	public double x;
	public double y;
	public double z;
	public double w;
}
