package math;

public class Vector2f {
	public Vector2f(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public Vector2f() {
		this.x = 0.0;
		this.y = 0.0;
	}
	
	public double module() {
		return Math.sqrt(x*x+y*y);
	}
	public double x;
	public double y;
}
