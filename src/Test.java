import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import game.GameConfiguration;
import game.cubes.Cube;
import game.cubes.GroundCube;
import math.Intersection;
import math.Ray;
import math.Vector3f;
import math.Vector3i;

class Test {

	@org.junit.jupiter.api.Test
	void testCubeRayIntersectionLookingSouth() {
		Vector3i cubePosition = new Vector3i(1, 1, 1);
		Cube cube = new GroundCube(cubePosition);
		
		Vector3f origin = new Vector3f(
				GameConfiguration.BLOCK_SIZE * 0.5,
				GameConfiguration.BLOCK_SIZE * 2.0,
				GameConfiguration.BLOCK_SIZE * 0.5);
		Vector3f direction = new Vector3f(0.0, -1.0, 0.0);
		Ray ray = new Ray(origin, direction);
		Intersection intersection = ray.intersect(cube);
		assertNotNull(intersection);
		assertEquals(GameConfiguration.BLOCK_SIZE, intersection.tHit);
		assertEquals(new Vector3f(GameConfiguration.BLOCK_SIZE * 0.5, GameConfiguration.BLOCK_SIZE, GameConfiguration.BLOCK_SIZE * 0.5), intersection.hitPoint);
		assertEquals(cube.north, ray.intersectCubeFace(cube));
	}
	
	@org.junit.jupiter.api.Test
	void testNoIntersectionLookingNorth() {
		Vector3i cubePosition = new Vector3i(1, 1, 1);
		Cube cube = new GroundCube(cubePosition);
		
		Vector3f origin = new Vector3f(
				GameConfiguration.BLOCK_SIZE * 0.5,
				GameConfiguration.BLOCK_SIZE * 2.0,
				GameConfiguration.BLOCK_SIZE * 0.5);
		Vector3f direction = new Vector3f(0.0, 1.0, 0.0);
		Ray ray = new Ray(origin, direction);
		Intersection intersection = ray.intersect(cube);
		assertNull(intersection);
	}
	
	@org.junit.jupiter.api.Test
	void testCubeRayIntersectionLookingNorth() {
		Vector3i cubePosition = new Vector3i(1, 1, 1);
		Cube cube = new GroundCube(cubePosition);
		
		Vector3f origin = new Vector3f(
				GameConfiguration.BLOCK_SIZE * 0.5,
				0,
				GameConfiguration.BLOCK_SIZE * 0.5);
		Vector3f direction = new Vector3f(0.0, 1.0, 0.0);
		Ray ray = new Ray(origin, direction);
		Intersection intersection = ray.intersect(cube);
		assertNotNull(intersection);
		assertEquals(0, intersection.tHit);
		assertEquals(new Vector3f(GameConfiguration.BLOCK_SIZE * 0.5, 0, GameConfiguration.BLOCK_SIZE * 0.5), intersection.hitPoint);
		assertEquals(cube.south, ray.intersectCubeFace(cube));
	}
	
	@org.junit.jupiter.api.Test
	void testCubeRayIntersectionHalfFromTop() {
		Vector3i cubePosition = new Vector3i(1, 1, 1);
		Cube cube = new GroundCube(cubePosition);
		
		Vector3f origin = new Vector3f(
				GameConfiguration.BLOCK_SIZE * 0.5,
				GameConfiguration.BLOCK_SIZE * 1.5,
				GameConfiguration.BLOCK_SIZE * 2);
		Vector3f direction = new Vector3f(0.0, -Math.sqrt(2) / 2, -Math.sqrt(2) / 2);
		Ray ray = new Ray(origin, direction);
		Intersection intersection = ray.intersect(cube);
		assertNotNull(intersection);
		assertEquals(Math.sqrt(2) * GameConfiguration.BLOCK_SIZE, intersection.tHit, 1E-3);
		assertEquals(new Vector3f(GameConfiguration.BLOCK_SIZE * 0.5, GameConfiguration.BLOCK_SIZE * 0.5, GameConfiguration.BLOCK_SIZE), intersection.hitPoint);
		assertEquals(cube.up, ray.intersectCubeFace(cube));
	}
}
