                ____________________________________________________
               /    ______                 __    __                /|
       	      /    / ____/                / /   /_/               / |
             /    / /_____  _____________/ /________________     / /
            /    / __  / / / / __  /  __/ __  / / __  / ___/    / /
           /     ___/ / /_/ / / / /__  / / / / / / / / ___/    / /
          /    /_____/_____/_/ /_/____/_/ /_/_/_/ /_/____/    / /
         /                                                   / /
        /     Web : www.sunshine2k.de                       / /    		
       /            www.bastian-molkenthin.de              / /
      /       Mail: webmaster@sunshine2k.de               / /
     /___________________________________________________/ /
     \____________________________________________________/


Triangle Rasterization Applet:
------------------------------

This little applet is my implementation of three algorithms on how to rasterize a triangle.


How to run the applet:
-----------------------
Just open the file TriangleRasterizationOnlyApplet.html in your preferred browser. (Java must be installed and enabled)


This is a Netbeans 7.1 project. You it should be no problem to compile the source from commandline with javac.

Sunshine, May 2012


